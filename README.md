# xlink-sdk-demo

> example for xlink-sdk

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

### <a name='env-wechat'>微信（设备）应用环境</a>
#### 把demo的地址换成相应的云智易应用APPID
![图1](./resource/image/qq.png)

#### 把demo的地址换成对应服务器地址
如果放在服务器根目录下则不需要更改，如放在服务器wechat目录下更改为/wechat/即可
![图1](./resource/image/server.png)
#### 在文件夹根目录执行npm install 命令

#### 在文件夹根目录执行npm build 命令

#### 把文件放置在服务器上面
把根目录的dist下的文件，放到服务器上面，如放在服务器根目录下的wechat，则把dist下面的所有文件放到该文件夹下面

#### 使用微信开发者工具访问页面
```
如放到服务器地址为http://test.cn的wechat文件夹下面则访问，且微信的公众号id为wx6bce565776a81ced
则访问地址为：
https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx6bce565776a81ced&redirect_uri=http://test.cn/wechat/index.html&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect
```
