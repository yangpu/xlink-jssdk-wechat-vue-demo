import axios from 'axios'
import _ from 'lodash'

function _getDefaultOptions () {
  return {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Token': window.localStorage.getItem('accessToken')
    }
  }
}

export default {
  post (url, data, options) {
    return axios.post(url, data, _.assign(_getDefaultOptions(), options))
  },
  get (url, options) {
    return axios.get(url, _.assign(_getDefaultOptions(), options))
  },
  put (url, data, options) {
    return axios.put(url, data, _.assign(_getDefaultOptions(), options))
  },
  del (url, options) {
    return axios.delete(url, _.assign(_getDefaultOptions(), options))
  }
}
