import http from './http'
// 正式平台
// let baseUrl = 'http://wx.xlink.cn'
// let platformUrl = 'http://api2.xlink.cn' // 平台URL
// let XLINKAPPID = '2e0fa6b3bb0edc00'// 云智易平台应用AppId

let baseUrl = 'http://139.196.164.250:8081'
let platformUrl = 'http://api-test.xlink.io:1080' // 平台URL
let XLINKAPPID = '2e07d2b1f2cdae00'// 云智易平台应用AppId

// 获取url上的code参数
function getSearchCode () {
  let search = location.href
  let searchs = {}
  let strs
  if (search.indexOf('?') !== -1) {
    search = search.substr(search.indexOf('?') + 1)
    strs = search.split('&')
    for (let i = 0; i < strs.length; i++) {
      searchs[strs[i].split('=')[0]] = decodeURIComponent(strs[i].split('=')[1])
    }
  }
  window.localStorage.setItem('code', searchs['code'])
  return searchs['code']
}

export default {
  /**
   * 微信登录认证
   * 认证登录获得RESTful调用凭证，通过调用凭证可调用企业信息管理接口，获取openId
   */
  getOpenId () {
    let code = getSearchCode()
    return http.get(`${baseUrl}/v2/wechat_gateway/${XLINKAPPID}/get_open_id?code=${code}`)
  },
  /**
   * 获取tooken
   */
  getUserAuthorize (OPENID) {
    return http.get(`${baseUrl}/v2/wechat_gateway/${XLINKAPPID}/wx_access_token?open_id=${OPENID}&resource=wechat`)
  },

  // 获取用户绑定设备列表
  getDeviceList () {
    return http.get(`${baseUrl}/v2/wechat_gateway/${XLINKAPPID}/wx_device_list?access_token=XLINKUserAccessToken&open_id=${window.localStorage.getItem('openId')}`)
  },

  /**
   * 获取虚拟设备数据
   * @param  {String} productId 产品Id
   * @param  {String} deviceId 虚拟设备 id
   * @return {Promise}
   */
  getVDevice (productId, deviceId, options) {
    return http.get(
      `${platformUrl}/v2/product/${productId}/v_device/${deviceId}`, options
    )
  }
}
