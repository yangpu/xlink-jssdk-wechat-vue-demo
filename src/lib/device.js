import EventEmitter from 'events'

function Device () {
  EventEmitter.call(this)
}

// 原型琏继承
Object.setPrototypeOf(Device.prototype, EventEmitter.prototype)

export default Device
