'use strict'

var SDKEvent = {
  READY: 'ready', // SDK 就绪，可以扫描设备
  STATUSCHANGE: 'statuschange', // SDK状态，即蓝牙和WIFI状态改变事件 见SDKStatus
  STARTSCAN: 'startscan', // 开始扫描设备
  CANCELSCAN: 'cancelscan', // 取消扫描设备
  DEVICESREADY: 'devicesready', // 添加或扫描设备完成
  ADDDEVICES: 'adddevices', // 添加设备
  DESTORY: 'destory', // 销毁一个连接实例
  UIBACK: 'uiback', // android系统的后退按钮事件
  TRACELOGS: 'traceLogs', // 添加设备日志事件请求
  TRACELOG: 'traceLog', // 设备日志事件
  DISCONNECT: 'disconnect', // 断开连接
  TRACESTATUS: 'traceStatus', // 设备日志事件输出状态
  ERROR: 'error'
}

var deviceEvent = {
  LOG: 'log',
  CONNECT: 'connect', // 连接设备
  DISCONNECT: 'disconnect', // 断开设备连接
  DATA: 'data', // 设备数据更新
  PROBE: 'probe', // 查询设备状态
  ALERT: 'alert', // 设备告警
  SUBSCRIBE: 'subscribe', // 订阅设备
  UNSUBSCRIBE: 'unsubscribe', // 取消订阅设备
  SENDDATA: 'senddata', // 发送设备数据
  STATUSCHANGE: 'statuschange', // 设备状态改变，上线(1)或者下线(0) 见deviceStatus
  ERROR: 'error'
}

var SDKType = {
  WIFI: 'wifi',
  BLUETOOTH: 'bluetooth',
  WEBSOCKET: 'websocket',
  MQTT: 'mqtt'
}

var errorCode = {
  LOGINFAIL: 0 // 登录失败
}

var deviceStatus = {
  ONLINE: 1,
  OFFLINE: 0
}

var subscribeType = {
  sync: '$6', // 设备上报数据端点!（订阅）
  setPoint: '$8', // 应用设置数据端点结果!（订阅）
  getPoint: '$a', // 应用获取数据端点结果（订阅）
  notifyEvent: '$d', // 应用通知事件
  event: '$e', // 系统事件
  sub: '$g', // 订阅设备结果（订阅）
  log: '$k', // 设备日志!（订阅）
  ticket: '$m', // 设备返回Ticket（订阅）
  getPrope: '$x' // 应用向设备获取数据端点
}
var sendType = {
  setPoint: '$7', // 应用设置/发送数据端点（发送）
  getPoint: '$9', // 应用获取数据端点（发送）
  offline: '$b', // 应用下线（发送）
  sub: '$f', // 订阅设备(发送)
  ticket: '$l', // 获取设备Ticket（发送）
  sendPrope: '$w'
}

export {SDKEvent, SDKType, deviceEvent, errorCode, deviceStatus, subscribeType, sendType}
