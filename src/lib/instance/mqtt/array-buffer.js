import { dataPointType, pad, _bytesToTimestamp, _bytesToInt, _bytesToNum, _bytesToFloat, _bytesToHex, _bytesToString, _stringToBytes, msgText } from './pad'
import { clone } from '../../util/lang'
/**
 * 同步数据端点,设备上报数据端点
 * @param  {Array} bytes 字节数组
 */
export function syncDatapoints (bytes) {
  // 第5位为数据端点上报标识位，占一个字节
  let flag = pad(bytes[4].toString(2), 8)
  let index = 5

  // 数据端点上报标识位最高位为设备名称标识
  if (flag[0] === '1') {
    console.log('设备端点标识')
    // 其他字节的前两位表示设备名称长度(两个字节)
    // let nameLen = _bytesToInt(bytes.splice(4, 6))
    let nameLen = _bytesToInt(bytes.slice(5, 7))

    index += nameLen + 2
  }

  if (flag[4] === '1') {
    // 时间戳占8位
    // let timestamp = this._bytesToTimestamp(bytes.slice(index, index + 8))
    index += 8
  }

  let datapointBytes = bytes.slice(index)
  let i = 0
  let datapointArr = []

  while (i < datapointBytes.length) {
    // 1.获取数据端点的索引
    let datapointIndex = datapointBytes[i]
    let arr = []
    // 2.获取两个字节，得到数据端点的类型和数据端点的长度
    datapointBytes.slice(i + 1, i + 3).forEach(item => {
      arr.push(pad(item.toString(2), 8))
    })
    let str = arr.join('')
    let type = parseInt(str.slice(0, 4), 2)
    let len = parseInt(str.slice(4), 2)
    // 3.拿到数据端点内容
    let value = datapointBytes.slice(i + 3, i + 3 + len)
    switch (type) {
      case 9: // 字符串
        value = _bytesToString(value)
        break
      case 10: // 字节数组转16进制
        value = _bytesToHex(value)
        break
      case 7: // 浮点数
        value = _bytesToFloat(value)
        break
      default:  // 整型
        value = _bytesToNum(value, type)
    }

    datapointArr.push({
      index: datapointIndex,
      value,
      type: dataPointType[type] || type
    })
    i = i + len + 3
  }
  return datapointArr
}

/**
 * 应用获取数据端点结果
 * @param  {Array} bytes 字节数组
 */
export function getDataPointResult (bytes) {
  // 服务不可用，获取失败，返回不附带Datapoint Flag、Datapoint Value
  if (bytes[6] === 1) {
    return
  }
  // 第7位为数据端点上报标识位，占一个字节
  let flag = pad(bytes[7].toString(2), 8)
  let index = 8

  // 数据端点上报标识位最高位为设备名称标识
  if (flag[0] === '1') {
    // 其他字节的前两位表示设备名称长度(两个字节)
    let nameLen = _bytesToInt(bytes.slice(8, 10))
    index += nameLen + 2
  }

  // 判断是否有数据端点
  if (flag[1] === 0) {
    return []
  }

  let datapointBytes = bytes.slice(index)
  let i = 0
  let datapointArr = []

  while (i < datapointBytes.length) {
    let datapointIndex = datapointBytes[i]
    let arr = []
    datapointBytes.slice(i + 1, i + 3).forEach(item => {
      arr.push(pad(item.toString(2), 8))
    })
    let str = arr.join('')
    let type = parseInt(str.slice(0, 4), 2)
    let len = parseInt(str.slice(4), 2)
    let value = datapointBytes.slice(i + 3, i + 3 + len)
    switch (type) {
      case 9: // 字符串
        value = _bytesToString(value)
        break
      case 10: // 字节数组
        value = _bytesToHex(value)
        break
      case 7: // 浮点数
        value = _bytesToFloat(value)
        break
      default:
        value = _bytesToNum(value, type)
    }

    datapointArr.push({
      index: datapointIndex,
      value,
      type: dataPointType[type] || type
    })
    i = i + len + 3
  }
  return datapointArr
}

/**
 * 应用获取数据端点结果(v6 prope协议)
 * @param  {Array} bytes 字节数组
 */
export function getPropeDataPointResult (bytes) {
  // 应用获取数据端点标志位（Probe Flag）
  let flag = pad(bytes[6].toString(2), 8)
  let index = 7

  // 数据端点上报标识位最高位为设备名称标识
  if (flag[1] === '1') {
    index += 8
  }

  // 判断是否有数据端点
  if (flag[0] === '0') {
    return []
  }

  let datapointBytes = bytes.slice(index)
  let i = 0
  let datapointArr = []

  while (i < datapointBytes.length) {
    let datapointIndex = datapointBytes[i]
    let arr = []
    datapointBytes.slice(i + 1, i + 3).forEach(item => {
      arr.push(pad(item.toString(2), 8))
    })
    let str = arr.join('')
    let type = parseInt(str.slice(0, 4), 2)
    let len = parseInt(str.slice(4), 2)
    let value = datapointBytes.slice(i + 3, i + 3 + len)
    switch (type) {
      case 9: // 字符串
        value = _bytesToString(value)
        break
      case 10: // 字节数组
        value = _bytesToHex(value)
        break
      case 7: // 浮点数
        value = _bytesToFloat(value)
        break
      default:
        value = _bytesToNum(value, type)
    }

    datapointArr.push({
      index: datapointIndex,
      value,
      type: dataPointType[type] || type
    })
    i = i + len + 3
  }
  return datapointArr
}

/**
 * 处理数据端点设置，应用设置数据端点
 * @param  {Array} bytes 字节数组
 */
export function setDataPointResult (bytes) {
  let result = {
    '0': {
      status: 0,
      msg: 'datapoint set successfully'
    },
    '1': {
      status: 1,
      msg: 'datapoint Setup failed'
    },
    '2': {
      status: 2,
      msg: 'no set permissions are granted'
    }
  }
  return result[bytes[6]] || bytes[6]
}

/**
 * 应用通知事件
 * @param  {Array} bytes 字节数组
 */
export function notifyEvent (bytes) {
  let source = {
    '1': 'event from server',
    '2': 'event from device',
    '3': 'event form app'
  }
  let eventSource = source[bytes[4]] || bytes[4] || ''
  let result = {
    '1': {
      status: 1,
      msg: '设备通知,数据端点变化通知'
    },
    '2': {
      status: 2,
      msg: '设备告警，数据端点变化引起的告警'
    },
    '3': {
      status: 3,
      msg: '设备分享，设备管理员发出的分享'
    },
    '4': {
      status: 4,
      msg: '消息广播推送'
    },
    '5': {
      status: 5,
      msg: '设备属性变化通知'
    },
    '6': {
      status: 6,
      msg: '设备与用户订阅关系变化通知'
    },
    '7': {
      status: 7,
      msg: '设备在线状态变化通知'
    },
    '8': {
      status: 8,
      msg: '设备在线状态告警'
    },
    '9': {
      status: 9,
      msg: '家庭消息通知，留言板消息'
    },
    '10': {
      status: 10,
      msg: '家庭邀请通知'
    },
    '11': {
      status: 11,
      msg: '家庭设备权限变化'
    },
    '12': {
      status: 12,
      msg: '家庭成员变化'
    },
    '13': {
      status: 13,
      msg: '家庭设备变化'
    }
  }
  let type = _bytesToInt(bytes.slice(9, 11))
  let response = result[type] || {}
  response.source = eventSource
  return response
}

/**
 * 系统事件
 * @param  {Array} bytes 字节数组
 */
export function systemEvent (bytes) {
  let result = {
    '1': {
      status: 1,
      msg: '升级指令'
    },
    '2': {
      status: 2,
      msg: '升级完成'
    },
    '3': {
      status: 3,
      msg: '请求服务器时间'
    },
    '4': {
      status: 4,
      msg: '服务器时间应答'
    },
    '5': {
      status: 5,
      msg: '强制踢下线'
    }
  }
  let type = _bytesToInt(bytes.slice(4, 6))
  return result[type] || type
}

/**
 * 写入设备日志
 * @param  {Array} bytes 字节数组
 */
export function log (bytes) {
  // [0, 20, 0, 35, 0, 3, 89, 220, 55, 43, 0, 27, 54, 55, 50, 57, 56, 52, 51, 48, 53, 35, 46, 35, 70, 48, 70, 69, 54, 66, 55, 57, 50, 54, 52, 57, 35, 46, 35]
  // 11点左右
  // [0, 20, 0, 35, 0, 3, 89, 220, 55, 217, 0, 27, 54, 55, 50, 57, 56, 52, 51, 48, 53, 35, 46, 35, 70, 48, 70, 69, 54, 66, 55, 57, 50, 54, 52, 57, 35, 46, 35]
  // 第5到第6位为日志等级和日志内容，需要转成16进制，拼成字符串再转回10进制
  let arr = []
  bytes.slice(4, 6).forEach(item => {
    arr.push(pad(item.toString(2), 8))
  })
  let str = arr.join('')
  let level = parseInt(str.slice(0, 4), 2)
  let type = parseInt(str.slice(4), 2)

  // 第7到第14位为时间戳
  let timestamp = _bytesToTimestamp(bytes.slice(6, 14))

  let msgArr = _bytesToString(bytes.slice(16)).split('#.#')

  return {
    timestamp,
    level,
    msg: msgText(type, msgArr)
  }
}

/**
 * ********************************************app发起的行为*****************************************************
 * */

/**
 * 获取数据端点
 * @export
 * @param {any} msgId
 * @returns {any} msgId
 */
export function getDataPoint (msgId) {
  // 7个固定的数据长度
  let b = new ArrayBuffer(7)
  let v1 = new Uint8Array(b)
  let len = pad((3).toString(2), 16)
  /*
   * arraybufferArray（用整型存储）
   * 数据包类型，用两个字节来存储
   * 0： 默认0
   * 1： 默认为7
   *
   * 数据包长度，就是内存的字节数，用两个字节来存储
   * 2：数据包高位
   * 3：数据包低位
   *
   * messageID
   * 4：message高位
   * 5：message低位
   *
   * 标志位
   * 6
   *
   * 数据包内容
   * 7 数据端点索引
   * 8  前四位代表type类型
   * 9：后12位代表数据内容的长度（字节）
   * 10 数据包内容
   */
  v1[0] = 0
  v1[1] = 9
  v1[2] = parseInt(len.slice(0, 8), 2)
  v1[3] = parseInt(len.slice(8, 16), 2)
  v1[4] = 0
  v1[5] = msgId++
  v1[6] = parseInt('10000000', 2)
  // 把内存发过去
  return {
    b,
    msgId
  }
}

/**
 * 获取数据端点(v6 prope协议)
 * @export
 * @param {array} array 数据索引的列表
 * @param {any} msgId
 * @returns {any} msgId
 */
export function getPropeDataPoint (array = [], msgId) {
  let dataPointLen = array.length
  let b = new ArrayBuffer(8 + dataPointLen)
  let v1 = new Uint8Array(b)
  // 数据包的长度
  let len = pad((4 + dataPointLen).toString(2), 16)
  /*
   * arraybufferArray（用整型存储）
   * 数据包类型，用两个字节来存储
   * 0： 默认0
   * 1： 默认为7
   *
   * 数据包长度，就是内存的字节数，用两个字节来存储
   * 2：数据包高位
   * 3：数据包低位
   *
   * messageID
   * 4：message高位
   * 5：message低位
   *
   * 标志位
   * 6
   *
   * 数据包内容
   * 7 数据端点索引
   * 8  前四位代表type类型
   * 9：后12位代表数据内容的长度（字节）
   * 10 数据包内容
   */
  v1[0] = 0
  v1[1] = 32
  v1[2] = parseInt(len.slice(0, 8), 2)
  v1[3] = parseInt(len.slice(8, 16), 2)
  v1[4] = 0
  v1[5] = msgId++
  v1[6] = parseInt('10000000', 2)
  v1[7] = dataPointLen
  array.forEach((item, index) => {
    v1[8 + index] = parseInt(item)
  })
  // 把内存发过去
  return {
    b,
    msgId
  }
}

/**
 * 设置数据端点数组类型
 * @export
 * @param {Array} array
 * @param {Number} msgId
 * @returns
 */
export function setDataPointArray (array, msgId) {
  // 后端定义的数据端点类型值跟硬件定义不一样，需要做个映射
  let arrlen = 0
  let arr = []
  array.forEach(item => {
    arr.push(clone(item))
  })
  arr.forEach((dp, index) => {
    let datapoint = ({
      '1': { // 布尔类型
        type: 0,
        len: 1
      },
      '2': { // 单字节(无符号)
        type: 0,
        len: 1
      },
      '3': { // 16位短整型(有符号)
        type: 1,
        len: 2
      },
      '4': { // 32位整型(有符号)
        type: 3,
        len: 4
      },
      '5': { // 浮点
        type: 7,
        len: 4
      },
      '6': { // 字符串
        type: 9
      },
      '7': { // 字节数组
        type: 10
      },
      '8': { // 16位短整型(无符号)
        type: 2,
        len: 2
      },
      '9': { // 32位整型(无符号)
        type: 4,
        len: 4
      }
    })[dp.type]

    if (dp.type === 1) { // 布尔值转换成0和1
      arr[index].value = dp.value ? 1 : 0
    }

    let valLen = datapoint.len || dp.value.length

    if (datapoint.type === 9) {
      // 字符串的长度
      valLen = _stringToBytes(dp.value).length
    } else if (datapoint.type === 10) {
      // 字节数组
      valLen = Math.ceil(dp.value.length / 2)
    }
    // 存储有用的数据（index, value, type,valLen）
    arr[index].valLen = valLen
    arr[index].type = datapoint.type
    // 获取整个端点数组的长度
    arrlen += valLen
    arrlen += 3
  })

  // 7个固定的数据端点长度，arrlen数据的长度，其中字节数组和字符串数组不确定
  let b = new ArrayBuffer(7 + arrlen)
  let v1 = new Int8Array(b)
  // 数据内容arrlen + 数据包实体内容3
  let len = pad((arrlen + 3).toString(2), 16)

  /*
   * arraybufferArray（用整型存储）
   * 数据包类型，用两个字节来存储
   * 0： 默认0
   * 1： 默认为7
   *
   * 数据包长度，就是内存的字节数，用两个字节来存储
   * 2：数据包低位
   * 3：数据包高位
   *
   * messageID
   * 4：message高位（一般不超过255，所有默认为0）
   * 5：message低位
   *
   * 标志位
   * 6 一般默认没有设备名称选项
   *
   * 数据端点内容
   * 7 数据端点索引
   * 8  前四位代表type类型
   * 9：后12位代表数据内容的长度（字节）
   * 10 数据包内容（n个字节）
   */
  v1[0] = 0
  v1[1] = 7
  v1[2] = parseInt(len.slice(0, 8), 2)
  v1[3] = parseInt(len.slice(8, 16), 2)
  v1[4] = 0
  v1[5] = msgId++
  v1[6] = parseInt('01100000', 2)
  let i = 7
  arr.forEach(dp => {
    // 每位数据端点的内容以及索引，长度
    v1[i] = dp.index
    let bin = `${pad(dp.type.toString(2), 4)}${pad(dp.valLen.toString(2), 12)}`
    v1[i + 1] = parseInt(bin.slice(0, 8), 2)
    v1[i + 2] = parseInt(bin.slice(8, 16), 2)
    // 从buffer的第十位开始创建一个新的进制存到内存（以大端字节方式写入）
    let v2 = new DataView(b, i + 3, dp.valLen)
    switch (dp.type) {
      case 0:
        v2.setUint8(0, dp.value, false)
        break
      case 1:
        v2.setInt16(0, dp.value, false)
        break
      case 2:
        v2.setUint16(0, dp.value, false)
        break
      case 3:
        v2.setInt32(0, dp.value, false)
        break
      case 4:
        v2.setUint32(0, dp.value, false)
        break
      case 7:
        v2.setFloat32(0, dp.value, false)
        break
      // 字符串
      case 9:
        let s = _stringToBytes(dp.value)
        s.forEach((item, index) => {
          v2.setUint8(index, item, false)
        })
        break
      // 字节数组
      case 10:
        // 一个字节存储两个字节数组的量（以16进制的方式）
        for (let i = 0; i < dp.valLen; i++) {
          let str = pad(dp.value.slice(i * 2, (i + 1) * 2), 2)
          v2.setInt8(i, parseInt(str, 16), false)
        }
        break
      default:
    }
    i = i + 3 + dp.valLen
  })
  // 把内存发过去
  return {
    b,
    msgId
  }
}

/**
 * 设备下线
 * @export
 * @returns
 */
export function setDeviceOffline () {
  let b = new ArrayBuffer(4)
  let v1 = new Int8Array(b)
  v1[0] = 0
  v1[1] = 11
  v1[2] = 0
  v1[3] = 4
  return b
}
