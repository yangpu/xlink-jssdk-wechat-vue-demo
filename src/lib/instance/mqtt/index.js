import Paho from './mqttws'
import md5 from 'md5'
import Device from '../../device'
import { SDKEvent, deviceEvent, subscribeType, sendType } from '../../enum'
import { isPlainObject, isArray, isFunction } from '../../util/lang'
import * as buffer from './array-buffer'

let _client = null // MQTT服务器
let _xsdk = null // 通过sdk事件暴露出去this
let _devices = {} // 保存每个设备的实例
let _msgId = 0

let userName = ''
let password = ''
let clientId = ''

/**
 * 初始化mqtt实例
 * @param {any} _xsdk
 * @param {Object} option
 */
function initMqttClient (xsdk, option) {
  if (!Paho || !Paho.MQTT) {
    throw new Error('MQTT do not exsit')
  }
  if (!option.type) {
    throw new Error('type do not exsit')
  }
  if (option.type === 'platform' && !option.token) {
    throw new Error('token do not exsit')
  }
  if (option.type === 'app' && (!option.userid || !option.authorize)) {
    throw new Error('userId or authorize do not exsit')
  }

  if (option.type === 'app') {
    userName = option.userid
    password = md5(`${option.userid}${option.authorize}`)
    // clientId = 'X:APP;A:4;V:1;'
    clientId = 'X:APP;A:4;V:2;'
  } else if (option.type === 'platform') {
    userName = option.token
    // clientId = 'X:APP;A:5;V:1;'
    clientId = 'X:APP;A:5;V:2;'
  }
  console.log(clientId)

  // 接收外面的实例
  _xsdk = xsdk

  // 开机事件
  _connectClient(option)

  // 监听外部的sdk事件
  _listenSdkEvent()
}

// 开机事件
function _connectClient (option) {
  if (!option.host || !clientId) {
    throw new Error('host or clientId do not exsit')
  }
  if (!_client) {
    // //建立客户端实例
    _client = new Paho.MQTT.Client(option.host, clientId)
    // 连接丢失
    _client.onConnectionLost = onConnectionLost
    // 接收到消息
    _client.onMessageArrived = onMessageArrived
  }

  let connectOption = {
    userName: userName,
    password: password,
    mqttVersion: 4,
    onSuccess () {
      _loginRecv()
    },
    onFailure (err) {
      if (_xsdk) {
        _xsdk.emit(SDKEvent.ERROR, new Error(err.errorMessage || '连接失败'))
        // 销毁实例
        _xsdk.emit(SDKEvent.DESTORY, false)
      }
    }
  }
  if (option && option.keepAliveInterval && typeof option.keepAliveInterval === 'number') {
    connectOption.keepAliveInterval = option.keepAliveInterval
  } else {
    connectOption.keepAliveInterval = 40
  }
  _client.connect(connectOption)
}

// 通知用户连接成功
function _loginRecv () {
  if (!_xsdk) {
    return
  }
  _xsdk.emit(SDKEvent.READY) // 1设备准备就绪
}

// 监听sdk事件
function _listenSdkEvent () {
  _xsdk.on(SDKEvent.ADDDEVICES, function (deviceLists) { // 1. 添加设备事件
    _createDevices(deviceLists)
  }).on(SDKEvent.DESTORY, function (boolean) { // 销毁mqtt事件,关机事件
    _distorySdk(boolean)
  })
}

/**
 * 订阅事件在订阅的时候写好事件的处理和回调
 * @param {Object} topic
 * @param {function} successEvent
 * @param {function} errorEvent
 * @returns
 */
function _subscribe (topic, successEvent, errorEvent) {
  _client.subscribe(topic, {
    onSuccess (res) {
      if (successEvent && isFunction(successEvent)) {
        successEvent()
      }
    },
    onFailure (err) {
      if (errorEvent && isFunction(errorEvent)) {
        errorEvent({
          status: 0,
          err,
          msg: `订阅${topic}失败`
        })
        console.error(`订阅${topic}失败`)
      }
    }
  })
}

// 销毁实例并且关闭mqtt, boolean为true开机成功
function _distorySdk (boolean = true) {
  if (_client && boolean) {
    _client.disconnect()
  }
  _devices = {}
  _client = null
  _xsdk = null
}

/**
 * MQTT处理连接丢失
 * @param  {Object} res 响应体
 */
function onConnectionLost (res) {
  // 0为正常关机的丢失,销毁实例的时候就是正常关机
  if (res.errorCode !== 0 && _xsdk) {
    _xsdk.emit(SDKEvent.ERROR, new Error(res.errorMessage || '连接丢失'))
    // 销毁实例
    _xsdk.emit(SDKEvent.DESTORY, false)
  }
}

/**
 * MQTT处理信息接收
 * @param  {Object} res 响应体
 */
function onMessageArrived (res) {
  if (res && res.destinationName) {
    let arr = res.destinationName.split('/')
    if (arr.length > 1) {
      let [topic, deviceId] = arr
      if (_devices[deviceId] && _devices[deviceId][topic]) {
        _devices[deviceId][topic](res)
      }
    }
  }
}

// 写进事件
function _writeEvent (topic, deviceId, callback) {
  if (_devices[deviceId] && !_devices[deviceId][topic] && callback && isFunction(callback)) {
    _devices[deviceId][topic] = function (message) {
      let result = null
      if (/\$6\/\d+/.test(message.destinationName)) { // 设备上报数据端点的回调1x
        result = buffer.syncDatapoints(message.payloadBytes)
      } else if (/\$8\/\d+/.test(message.destinationName)) { // 设置数据端点的结果1x
        result = buffer.setDataPointResult(message.payloadBytes)
      } else if (/\$a\/\d+/.test(message.destinationName)) { // 获取数据端点的订阅1x
        result = buffer.getDataPointResult(message.payloadBytes)
      } else if (/\$k\/\d+/.test(message.destinationName)) { // 设备日志1x
        result = buffer.log(message.payloadBytes)
      } else if (/\$x\/\d+/.test(message.destinationName)) {
        result = buffer.getPropeDataPointResult(message.payloadBytes)
        console.log(132414134)
        console.log(result)
      } else {
        result = message
      }
      // 执行对应的回调函数并且把结果传出去
      callback(result)
    }
  }
}

// 发送数据端点
function _sendMessage (params, callback) {

  let event = `${sendType[params.topic]}/${params.deviceId}`
  let memory = null

  if (/\$7\/\d+/.test(event)) {
    // 发送数据端点
    if (!params.data || !params.data.length) return
    let { b, msgId } = buffer.setDataPointArray(params.data, _msgId)
    _msgId = msgId
    memory = b
  } else if (/\$9\/\d+/.test(event)) {
    // 应用获取数据端点
    let { b, msgId } = buffer.getDataPoint(_msgId)
    _msgId = msgId
    memory = b
  } else if (/\$w\/\d+/.test(event)) {
    // prope获取数据端点
    let { b, msgId } = buffer.getPropeDataPoint(params.data, _msgId)
    _msgId = msgId
    memory = b
    console.log(b)
  }
  if (!memory) return
  let message = new Paho.MQTT.Message(memory)
  message.destinationName = event
  message.qos = 1
  _client.send(message)

  if (callback && isFunction(callback)) {
    callback()
  }
}

// 创造一个设备监听的类
function _createDevices (deviceLists) {
  if (deviceLists && deviceLists.length) {
    let added = []
    deviceLists.forEach((item) => {
      if (_devices[item.device_id] && _devices[item.device_id] instanceof Device) {
        added.push(_devices[item.device_id])
      } else {
        let device = new Device()
        device.id = item.device_id
        device.mac = item.device_mac
        device.pid = item.device_pid
        _devices[device.id] = device
  
        // 1.发送数据端点需要sendMessage
        device.on(deviceEvent.SENDDATA, function (data, cb) {
          _sendData(this.id, data, cb)
        })
        // 2.请求获取所有数据端点需要sendMessage
        device.on(deviceEvent.PROBE, function (cb) {
          _probeDevice(this.id, cb)
          _probegetData(item.device_id)
        })
  
        // 3.设备日志
        device.on(deviceEvent.LOG, function (cb) {
          // _log(this.id, cb)
        })

        // 4.订阅设备上报
        deviceSubscribe(item.device_id)

        added.push(device)
      }   
    })
    _xsdk.emit(SDKEvent.DEVICESREADY, added)
  }
}

// 获取数据端点
function _probeDevice (deviceId, callback) {
  if (!_client || !_xsdk) {
    throw new Error('xsdk not init')
  }
  let topic = subscribeType['getPoint']
  if (_devices[deviceId] && !_devices[deviceId][topic]) {
    _subscribe(`${topic}/${deviceId}`, () => {
      // 1.写进事件
      _writeEvent(topic, deviceId, (res) => {
        if (callback && isFunction(callback)) {
          callback({data: res, deviceId})
        }
      })

      // 2.订阅成功发送自动获取数据端点
      _sendMessage({topic: 'getPoint', deviceId})
    }, (err) => {
      // 订阅失败
      if (_devices && _devices[deviceId]) {
        _devices[deviceId].emit(deviceEvent.ERROR, err)
      }
    })
  } else {
    // 订阅成功发送自动获取数据端点
    _sendMessage({topic: 'getPoint', deviceId})
  }
}

// 设备上报数据端点
function _pipeRecv (data, deviceId) {
  if (!deviceId || !_devices[deviceId]) {
    return
  }
  let device = _devices[deviceId]
  device.emit(deviceEvent.DATA, {
    type: 'datapoint',
    data,
    deviceId
  })
}

// 返回设备日志
function _log (deviceId, callback) {
  if (!deviceId || !_devices[deviceId]) {
    return
  }
  let topic = subscribeType['log']
  if (_devices[deviceId] && !_devices[deviceId][topic]) {
    _subscribe(`${topic}/${deviceId}`, () => {
      // 1.写进事件,方便全局调用获取
      _writeEvent(topic, deviceId, (data) => {
        if (callback && isFunction(callback)) {
          callback({
            data,
            deviceId
          })
        }
      })
    }, (err) => {
      // 订阅失败
      if (_devices && _devices[deviceId]) {
        _devices[deviceId].emit(deviceEvent.ERROR, err)
      }
    })
  }
}

// 发送数据端点
function _sendData (deviceId, data, callback) {
  if (!_client || !_xsdk) {
    throw new Error('xsdk not init')
  }
  if (!isPlainObject(data) && isArray(data.data)) {
    throw new Error('params error')
  }

  if (data.type === 'datapoint' && isArray(data.data)) { // 设置数据端点
    // 订阅发送数据端点
    let topic = subscribeType['setPoint']
    if (_devices[deviceId] && !_devices[deviceId][topic]) {
      _subscribe(`${topic}/${deviceId}`, () => {
        // 写进事件,方便全局调用获取
        _writeEvent(topic, deviceId, (res) => {
          if (callback && isFunction(callback)) {
            callback(res)
          }
        })
      }, (err) => {
        // 订阅失败
        if (_devices && _devices[deviceId]) {
          _devices[deviceId].emit(deviceEvent.ERROR, err)
        }
      })
    }

    // 发送数据端点
    _sendMessage({
      topic: 'setPoint',
      deviceId,
      data: data.data
    })
  }
}

/**
 * 订阅主题，并且做处理
 * 设备上报数据端点
 */
function deviceSubscribe (deviceId) {
  if (!_xsdk || !deviceId) return
  let topic = subscribeType['sync']
  if (_devices[deviceId] && !_devices[deviceId][topic]) {
    _subscribe(`${topic}/${deviceId}`, () => {
      // 1.写进事件,方便全局调用获取
      _writeEvent(topic, deviceId, (data) => {
        // data是返回的数据
        _pipeRecv(data, deviceId)
      })
    }, (err) => {
      // 订阅失败
      if (_devices && _devices[deviceId]) {
        _devices[deviceId].emit(deviceEvent.ERROR, err)
      }
    })
  }
}

/**
 * 订阅主题，并且做处理
 * 设备上报数据端点
 */
function _probegetData (deviceId, callback) {
  if (!_client || !_xsdk) {
    throw new Error('xsdk not init')
  }
  let topic = subscribeType['getPrope']
  if (_devices[deviceId] && !_devices[deviceId][topic]) {
    _subscribe(`${topic}/${deviceId}`, () => {
      // 1.写进事件
      _writeEvent(topic, deviceId, (res) => {
        if (callback && isFunction(callback)) {
          callback({data: res, deviceId})
        }
      })
      console.log('prope success')

      // 2.订阅成功发送自动获取数据端点
      _sendMessage({topic: 'sendPrope', deviceId, data: [1, 2, 3]})
    }, (err) => {
      // 订阅失败
      console.log('订阅prope topic 失败')
      if (_devices && _devices[deviceId]) {
        _devices[deviceId].emit(deviceEvent.ERROR, err)
      }
    })
  } else {
    // 订阅成功发送自动获取数据端点
    _sendMessage({topic: 'sendPrope', deviceId, data: [1]})
  }
}

export default initMqttClient
