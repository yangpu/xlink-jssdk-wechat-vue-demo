/**
 * 在前面补0
 * @param  {Number|String} num 需要补零的数字
 * @param  {Number} n   长度
 * @return {String}
 */
export function pad (num, n) {
  let len = num.toString().length
  while (len < n) {
    num = '0' + num
    len++
  }
  return num
}

/**
 * 字节数组转时间戳，需要转成16进制，拼成字符串再转回10进制
 * @param  {Array}  bytes 字节数组
 * @param  {Boolean} ms 是否毫秒
 * @return {String}
 */
export function _bytesToTimestamp (bytes, ms = false) {
  // let result = _bytesToInt(bytes)
  // return ms ? result * 1000 : result
  return _bytesToInt(bytes)
}

/**
 * 字节数组转数字
 * @param  {Array}  bytes 字节数组
 * @return {String}
 */
export function _bytesToInt (bytes) {
  let arr = []
  bytes.forEach(item => {
    arr.push(pad(item.toString(2), 8))
  })
  return parseInt(arr.join(''), 2)
}

/**
 * 字节数组转数字
 * @param  {Array}  bytes 字节数组
 * @return {String}
 */
export function _bytesToNum (bytes, type) {
  let res = 0
  let ab
  let view
  switch (type) {
    case 0: // 单字节
      res = bytes[0]
      break
    case 1: // 16位有符号
      ab = new ArrayBuffer(2)
      view = new Int8Array(ab)
      view[1] = bytes[0]
      view[0] = bytes[1]
      let int16view = new Int16Array(ab)
      res = int16view[0]
      break
    case 2: // 16位无符号
      ab = new ArrayBuffer(2)
      view = new Int8Array(ab)
      view[1] = bytes[0]
      view[0] = bytes[1]
      let uint16view = new Uint16Array(ab)
      res = uint16view[0]
      break
    case 3: // 32位有符号
      ab = new ArrayBuffer(4)
      view = new Int8Array(ab)
      view[3] = bytes[0]
      view[2] = bytes[1]
      view[1] = bytes[2]
      view[0] = bytes[3]
      let int32view = new Int32Array(ab)
      res = int32view[0]
      break
    case 4: // 32位无符号
      ab = new ArrayBuffer(4)
      view = new Int8Array(ab)
      view[3] = bytes[0]
      view[2] = bytes[1]
      view[1] = bytes[2]
      view[0] = bytes[3]
      let uint32view = new Uint32Array(ab)
      res = uint32view[0]
      break
    default:
  }
  return res
}

/**
 * 字节数组转浮点数
 * @param  {Array}  bytes 字节数组
 * @return {Number}
 */
export function _bytesToFloat (bytes) {
  let arr = []
  bytes.forEach(byte => {
    arr.push(pad(byte.toString(16), 2))
  })
  let a = arr.join('').match(/.{1,2}/g).map(num => {
    return parseInt(num, 16)
  }).reverse()
  let uint8Array = new Uint8Array(a).buffer
  return new Float32Array(uint8Array)[0]
}

/**
 * 字节数组转十六进制
 * @param  {Array}  bytes 字节数组
 * @return {String}
 */
export function _bytesToHex (bytes) {
  let arr = []
  bytes.forEach(item => {
    arr.push(pad(item.toString(16), 2).toUpperCase())
  })
  return arr.join('')
}

/**
 * 字节数组转字符串
 * @param  {Array}  bytes 字节数组
 * @return {String}
 */
export function _bytesToString (bytes) {
  // return String.fromCharCode.apply(null, bytes)
  var encodedString = String.fromCharCode.apply(null, bytes)
  let decodedString = decodeURIComponent(escape(encodedString))
  return decodedString
}

/**
 * 字符串转字节数组
 * @param  {String}  str 字符串
 * @return {Array}
 */
export function _stringToBytes (str) {
  str = unescape(encodeURIComponent(str))
  let charList = str.split('')
  let uintArray = []
  for (var i = 0; i < charList.length; i++) {
    uintArray.push(charList[i].charCodeAt(0))
  }
  return uintArray
}

/**
 * 消息文案
 * @param  {[type]} type 消息类型
 * @param  {[type]} msg  用于填充消息占位符的数组
 */
export function msgText (type, msg) {
  const LOG_TEMPLATES = {
    '1': '设备[ID:{0}, MAC:{1}]上线了, 上线IP为:{2}',
    '2': '设备[ID:{0}, MAC:{1}]下线了',
    '3': '设备[ID:{0}, MAC:{1}]发送Ping',
    '4': '设备[ID:{0}, MAC:{1}]接收到设置数据端点指令',
    '5': '设备[ID:{0}, MAC:{1}]返回了设置数据端点指令, 设置返回码为{2}',
    '6': '设备[ID:{0}, MAC:{1}]接收了应用{2}的订阅请求',
    '7': '设备[ID:{0}, MAC:{1}]返回了应用{2}的订阅, 返回码为{3}',
    '8': '设备[ID:{0}, MAC:{1}]上报了数据端点,内容为:{2}',
    '9': '设备[ID:{0}, MAC:{1}]收到了通知',
    '10': '设备[ID:{0}, MAC:{1}]完成了升级, 版本由{2}升级到了{3}'
  }
  const template = LOG_TEMPLATES[type]
  return template.replace(/\{(\d)\}/g, function (match, p1) {
    return msg[p1]
  })
}

export const dataPointType = {
  '1': 3, // 16位短整型(有符号)
  '2': 8, // 16位短整型(无符号)
  '3': 4, // 32位整型(有符号)
  '4': 9, // 32位整型(无符号)
  '7': 5, // 浮点
  '9': 6, // 字符串
  '10': 7 // 字节数组
}
