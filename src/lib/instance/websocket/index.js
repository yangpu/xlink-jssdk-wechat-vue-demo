import io from 'socket.io-client'
import {SDKEvent, deviceEvent, deviceStatus} from '../../enum'
import Device from '../../device'
import {isPlainObject, isArray, clone, isFunction} from '../../util/lang'
import debug from 'debug'
import base64 from '../../util/base64'
import protocol from './protocol'

var log = debug('xsdk-websocket')
var _socket
var _xsdk // 通过sdk事件暴露出去this
var _devices = {}
var _messages = {}
var _curMessageId = 0
var MAX_MESSAGE_SIZE = 256
var isPlatform = false

function initWebsocket (xsdk, option) {
  xsdk.host = option.host
  _xsdk = xsdk

  protocol.setDpVersion(option.dp_version)
  _createSocketIO(option)
  _listenSdkEvent()
}

function _createSocketIO (option) {
  log('init socket-io')

  if (!io) {
    throw new Error('socket.io do not exsit')
  }

  if (option.type && option.type === 'platform') {
    isPlatform = true
    // _socket = io.connect(option.host, {'force new connection': true})
    _socket = io(option.host, { forceNew: true })
  } else {
    _socket = io(option.host)
  }

  // 注册socket的状态的监听事件
  _socket.on('connect', function () {
    if (isPlatform) {
      _loginRecv()
    } else {
      _appLogin(option)
    }
  }).on('sync', function (data) { // 接受数据端点上报
    _syncDeviceInfo(data)
  }).on('app.login', function (data) { // 登录
    _loginRecv(data)
  }).on('set', function (data) { // 登录
    _setRecv(data)
  }).on('probe', function (data) { // 请求数据端点数据
    _probeRecv(data)
  }).on('pipe', function (data) { // 接受透传数据上报
    _pipeRecv(data)
  }).on('pipe_sync', function (data) { // 接受透传广播数据上报
    _pipeRecv(data)
  }).on('event.notify', function (data) { // 接受设备上下线状态
    _eventNotify(data)
  }).on('trace.log', function (data) { // 输入日志
    _outputLogs(data)
  }).on('trace.status', function (data) { // 输出状态
    _outputLogs(data)
  }).on('error', function (error) {
    _xsdk.emit(SDKEvent.ERROR, error)
  })
}

// 监听sdk的事件
function _listenSdkEvent () {
  _xsdk.on(SDKEvent.ADDDEVICES, function (deviceLists) { // 1. 添加设备事件
    _createDevices(deviceLists)
  }).on(SDKEvent.DESTORY, function () { // 销毁socket事件
    _distorySdk()
  })
}

// 销毁sdk
function _distorySdk () {
  if (!_socket) return
  if (isPlatform) {
    clearVariables()
  } else {
    _socket.emit(protocol.logout.events, clone(protocol.logout.params), function (data) {
      clearVariables()
    })
  }
}

// 清除变量
function clearVariables () {
  if (!_socket) return
  _socket.close()
  _socket = null
  _xsdk = null
  _devices = {}
}

// 创造一个设备监听的类
function _createDevices (deviceLists) {
  if (deviceLists && deviceLists.length) {
    var added = []
    deviceLists.forEach((item) => {
      var device = new Device()
      device.id = item.device_id
      // device.mac = item.device_mac
      // device.pid = item.device_pid
      device.on(deviceEvent.SENDDATA, function (data, cb) {
        _sendData(this.id, data, cb)
      })
      device.on(deviceEvent.PROBE, function (cb) {
        _probeDevice(this.id, cb)
      })

      device.on(deviceEvent.LOG, function (token, cb) {
        _log(this.id, token, cb)
      })
      added.push(device)
      _devices[device.id] = device
    })
    _xsdk.emit(SDKEvent.DEVICESREADY, added)
  }
}

function _probeDevice (deviceId, cb) {
  if (!_socket) {
    throw new Error('xsdk not init')
  }
  var params = clone(protocol.probe.params)
  params.device_id = deviceId
  params.message_id = _genMessageId()
  _messages[params.message_id] = cb
  _socket.emit(protocol.probe.events, params)
}

function _sendData (deviceId, data, cb) {
  if (!_socket) {
    throw new Error('xsdk not init')
  }
  if (!isPlainObject(data) && isArray(data.data)) {
    throw new Error('params error')
  }
  var params
  if (data.type === 'datapoint' && isArray(data.data)) { // 设置数据端点
    params = clone(protocol.set.params)
    params.device_id = deviceId
    params.message_id = _genMessageId()
    _messages[params.message_id] = cb
    params.datapoint = []

    data.data.forEach(function (item) {
      var temp = {}
      temp[item.index] = item.value
      params.datapoint.push(temp)
    })
    // 设置数据端点
    _socket.emit(protocol.set.events, params)
  } else if (data.type === 'pipe') { // 发送透传字符串
    params = clone(protocol.pipe.params)

    params.pipe = {
      encode: 'base64',
      data: base64.encode(data.data)
    }

    params.to_id = deviceId
    params.message_id = _genMessageId()
    _messages[params.message_id] = cb

    // 发送透传数据
    _socket.emit(protocol.pipe.events, params)
  }
}

// 返回设备日志
function _log (id, token, cb) {
  if (_socket) {
    if (!id || !token) {
      throw new Error('deviceId or token do not exsit')
    }
    _socket.emit('trace.logs', {
      id,
      token
    })
    _messages[`log${id}`] = cb
    _messages.status = cb
  }
}

// 输出设备日志
function _outputLogs (data) {
  var cb = null
  if (data && data.id) {
    cb = _messages[`log${data.id}`]
    data.type = 'log'
  } else if (data && data.status) {
    cb = _messages.status
    data.type = 'status'
  }
  if (!cb || !isFunction(cb)) {
    return
  }
  cb(data)
}

function _setRecv (data) {
  if (!data.is_resp) {
    return
  }
  var cb = _messages[data.message_id]
  if (!cb || !isFunction(cb)) {
    return
  }
  cb({
    status: data.errcode,
    msg: data.errmsg
  })
}

function _probeRecv (data) {
  if (!data.is_resp) {
    return
  }
  var cb = _messages[data.message_id]
  if (!cb || !isFunction(cb)) {
    return
  }
  if (data.errcode) { // 0表示成功 10 表示设备离线
    cb({
      status: data.errcode,
      msg: data.errmsg,
      deviceId: data.device_id
    })
    return
  }

  if (cb && data.data && data.data.datapoint && isArray(data.data.datapoint)) {
    var dps = []
    data.data.datapoint.forEach(function (item) {
      var temp = {
        index: item.index,
        value: item.value
      }
      dps.push(temp)
    })
    cb({
      status: 0,
      data: dps,
      deviceId: data.device_id
    })
  }
}

function _loginRecv () {
  if (!_xsdk) {
    return
  }
  _xsdk.emit(SDKEvent.READY) // 1设备准备就绪
}

function _eventNotify (data) {
  if (!(data && data.from && _devices[data.notify.data.device_id])) {
    return
  }
  var device = _devices[data.notify.data.device_id]
  if (data.notify.type === 2) { // 设备告警
    device.emit(deviceEvent.ALERT, data.notify.data)
  } else if (data.notify.type === 5 && (data.notify.data.type === 'offline' || data.notify.data.type === 'online')) { // 设备在线状态变化
    if (data.notify.data.type === 'offline') {
      device.emit(deviceEvent.STATUSCHANGE, deviceStatus.OFFLINE)
    } else {
      device.emit(deviceEvent.STATUSCHANGE, deviceStatus.ONLINE)
    }
  }
}

function _pipeRecv (data) {
  if (data.is_resp) { // 返回信息
    var cb = _messages[data.message_id]
    if (cb && isFunction(cb)) {
      cb({ // 0表示成功，其他失败
        status: data.errcode,
        msg: data.errmsg
      })
    }
    return
  }

  if (!data.from_id || !_devices[data.from_id]) {
    return
  }
  var device = _devices[data.from_id]
  if (data.pipe && data.pipe.encode === 'base64') {
    let decodedArr = base64.decode(data.pipe.data)
    device.emit(deviceEvent.DATA, {
      type: 'pipe',
      data: decodedArr,
      deviceId: device.id
    })
  }
  if (data.pipe_sync && data.pipe_sync.encode === 'base64') {
    let decodedArr = base64.decode(data.pipe_sync.data)
    device.emit(deviceEvent.DATA, {
      type: 'pipe_sync',
      data: decodedArr,
      deviceId: device.id
    })
  }
}

// 所有的获取数据端点
function _syncDeviceInfo (data) {
  if (!data.device_id || !_devices[data.device_id]) {
    return
  }
  var device = _devices[data.device_id]
  device.emit(deviceEvent.DATA, {
    type: 'datapoint',
    data: data.datapoint,
    deviceId: device.id
  })
}

function _appLogin (option) {
  let params = clone(protocol.appLogin.params)
  params.app_id = option.userid
  params.authorize_code = option.authorize
  _socket.emit(protocol.appLogin.events, params)
}

function _genMessageId () {
  _curMessageId++
  if (_curMessageId >= MAX_MESSAGE_SIZE) {
    _curMessageId = 0
  }
  return _curMessageId
}

export default initWebsocket
