var VERSION = 1
var dpVersion = 1
var protocol = {
  setDpVersion: function (version) {
    if (version && typeof version === 'number') {
      dpVersion = version
    }
  },
  getDpVersion: function () {
    return dpVersion
  },
  appLogin: {
    events: 'app.login',
    params: {
      is_resp: false,
      protocol_version: VERSION, // "协议版本",
      app_id: '', // 用户ID
      authorize_code: ''
    }
  },
  set: {
    events: 'set',
    params: {
      is_resp: false,
      device_id: '',
      dp_version: dpVersion,
      message_id: 0,
      datapoint: null
    }
  },
  pipe: {
    events: 'pipe',
    params: {
      is_resp: false,
      to_id: '',
      message_id: 0,
      pipe: null
    }
  },
  probe: {
    events: 'probe',
    params: {
      is_resp: false,
      device_id: '',
      message_id: 0,
      dp_version: dpVersion
    }
  },
  logout: {
    events: 'logout',
    params: {
      is_resp: false,
      reason: ''
    }
  }
}

for (var i in protocol) {
  if (i === 'set' || i === 'probe') {
    Object.defineProperty(protocol[i].params, 'dp_version', {
      get: protocol.getDpVersion,
      set: protocol.setDpVersion
    })
  }
}

export default protocol
