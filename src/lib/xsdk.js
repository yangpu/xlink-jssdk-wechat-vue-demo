'use strict'

import {SDKEvent, SDKType} from './enum'
import getXUI from './plugins/xui'
import getDataStorage from './plugins/data-storage'
import initWebsocket from './instance/websocket'
import initMqttClient from './instance/mqtt'
import {isString, isPlainObject} from './util/lang'
import EventEmitter from 'events'

var _instance = {} // 保存sdk实例，每种类型只保留一个实例

function XSDK (type, option) {
  if (!isString(type)) {
    throw new TypeError('error params')
  }
  if (type === SDKType.WEBSOCKET) { // 发送数据时，数据项不能为空
    if (option.type && option.type === 'platform') {
      if (!isPlainObject(option) || !isString(option.host)) {
        throw new TypeError('error params')
      }
    } else {
      if (!isPlainObject(option) || !isString(option.host) || !isString(option.userid) || !isString(option.authorize)) {
        throw new TypeError('error params')
      }
    }
  }
  // 借用构造函数继承
  EventEmitter.call(this)

  var alias = option ? type + '_' + option.host : type
  if (this instanceof XSDK) {
    if (_instance[alias] === undefined) {
      switch (type) {
        case SDKType.WIFI:
          break
        case SDKType.BLUETOOTH:
          break
        case SDKType.WEBSOCKET:
          initWebsocket(this, option)
          break
        case SDKType.MQTT:
          initMqttClient(this, option)
          break
        default:
          throw new Error(type + ' is not support')
      }
      this.type = type
      _instance[alias] = this
      _distorySdkBind(this, alias)
    } else {
      return _instance[alias]
    }
  } else {
    return new XSDK(type, option)
  }
}

function _distorySdkBind (sdk, alias) {
  // 监听实例的销毁
  sdk.on(SDKEvent.DESTORY, function () {
    delete _instance[alias]
  })
}

// 原型琏继承
Object.setPrototypeOf(XSDK.prototype, EventEmitter.prototype)

XSDK.getXUI = getXUI
XSDK.getDataStorage = getDataStorage

window.XSDK = XSDK

export default XSDK
