import Vue from 'vue'
import Router from 'vue-router'
import wechat from '@/components/app'

Vue.use(Router)
const router = new Router({
  routes: [
    {
      path: '/',
      name: 'wechat',
      component: wechat,
      meta: {
        title: '回收箱管理',
        requireAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  next()
})

// 在路由导航之后更新当前页面标题
router.afterEach(route => {
  // 从路由的元信息中获取 title 属性
  if (route.meta.title) {
    document.title = route.meta.title
  }
})

export default router
