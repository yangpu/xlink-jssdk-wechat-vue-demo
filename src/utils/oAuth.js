export default class OAuthUtils {
  constructor () {
    this.code = window.sessionStorage.getItem('code')
  }

  // 是否是刷新
  isRefsh () {
    if (window.sessionStorage.getItem('code')) {
      return window.sessionStorage.getItem('code') === getparam('code')
    } else {
      // 刚进来的时候没有session，不刷新
      return getparam('code') === null
    }
  }

  goOAuth (appId, redirectUrl) {
    redirectUrl = encodeURIComponent(redirectUrl)
    window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appId}&redirect_uri=${redirectUrl}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect`
  }
}

export const OAuthModules = new OAuthUtils()

// 获取地址栏上的参数
export function getparam (key) {
  var name
  let value
  var str = window.location.href // 取得整个地址栏
  var num = str.indexOf('?')
  str = str.substr(num + 1) // 取得所有参数   stringvar.substr(start [, length ]

  var arr = str.split('&') // 各个参数放到数组里
  for (var i = 0; i < arr.length; i++) {
    num = arr[i].indexOf('=')
    if (num > 0) {
      name = arr[i].substring(0, num)
      value = arr[i].substr(num + 1)
      if (key === name) {
        return value
      }
    }
  }
  return null
}
